package com.epam.sqlinjection;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.HashSet;
import java.util.OptionalLong;

public class LoginHandler {

    public OptionalLong login(Connection sqlConnection, String username, String password) throws SQLException {
        Collection<String> allUsernames = retrieveUsernames(sqlConnection);
        if (!allUsernames.contains(username)){
            return OptionalLong.empty();
        }

        final Statement statement = sqlConnection.createStatement();
        final ResultSet resultSet = statement.executeQuery(
                "select user_id, userpass from user " +
                        "where username = '" + username + "'");

        if (resultSet.next()) {
            String passwordFromDatabase = resultSet.getString("userpass");
            if (passwordFromDatabase.equals(password)){
                return OptionalLong.of(resultSet.getLong("user_id"));
            }
        }
        return OptionalLong.empty();
    }

    private Collection<String> retrieveUsernames(Connection connection) throws SQLException {
        Collection<String> usernames = new HashSet<>();
        final Statement statement = connection.createStatement();
        final ResultSet resultSet = statement.executeQuery("select username from user");
        while (resultSet.next()) {
            usernames.add(resultSet.getString("username"));
        }
        return usernames;
    }
}
